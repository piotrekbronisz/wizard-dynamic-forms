
var myApp = angular.module('myApp', ['fg', 'ngSanitize', 'markdown']);

myApp.controller('MyController', function($scope, mySchema) {

  $scope.myForm = {
    schema: mySchema
  };

});

myApp.value('mySchema', {
  "fields": [
    {
      "type": "text",
      "name": "name",
      "displayName": "Name",
      "validation": {
        "messages": {},
        "required": true
      },
      "placeholder": "Enter your name here",
      "tooltip": "Enter your name here"
    },


    {
      "type": "textarea",
      "name": "message",
      "displayName": "Message",
      "validation": {
        "messages": {},
        "required": true
      },
      "placeholder": "Enter your message here",
      "tooltip": "Enter your message here"
    }
    
  ]
});